# Simple CRUD CLI Python

- Python 3.8
- SQLite3

## Instalasi
- [download](https://gitlab.com/fajaralfa/cli-crud-python/-/archive/main/cli-crud-python-main.zip) app nya
- ekstrak
- buka terminal
- cd ke folder app nya
- jalankan

``` bash
python crud.py
```

## Table Schema
```sql
CREATE TABLE identity (
id INTEGER NOT NULL PRIMARY KEY,
name VARCHAR(255) NOT NULL,
address VARCHAR(255) NOT NULL
);

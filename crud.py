import sqlite3
import os


class CRUD:

    db = sqlite3.connect('./db.sqlite3')
    cursor = db.cursor()

    def create(self):
        name = ''
        address = ''
        try:
            name = input('Masukan Nama: ')
            address = input('Masukan Alamat: ')
        except KeyboardInterrupt:
            print('\nKembali.')
            return
        sql = 'INSERT INTO identity (name, address) VALUES (?,?)'
        self.cursor.execute(sql, [name, address])
        self.db.commit()
        print('Tambah Data Berhasil')
        pass

    def read(self):
        sql = 'SELECT * FROM identity'
        data = self.cursor.execute(sql)
        data = data.fetchall()
        for res in data:
            print(f'Id. {res[0]} | Nama. {res[1]} | Alamat. {res[2]}')
        pass

    def update(self):
        self.read()
        id = ''
        name = ''
        address = ''
        try:
            id = input('Masukan Id Target: ')
            id = int(id)
            name = input('Masukan Nama: ')
            address = input('Masukan Alamat: ')
            sql = 'UPDATE identity SET name = ?, address = ? WHERE id = ?'
            self.cursor.execute(sql, [name, address, id])
            self.db.commit()
            print('Edit Data Berhasil')
        except KeyboardInterrupt:
            print('\nKembali.')
            return
        except ValueError:
            print('Id tidak valid')
            self.update()
        pass

    def delete(self):
        self.read()
        try:
            id = input('Masukan Id: ')
            id = int(id)
            sql = 'DELETE FROM identity WHERE id = ?'
            self.cursor.execute(sql, [id])
            self.db.commit()
            print('Hapus Data Berhasil')
        except KeyboardInterrupt:
            print('\nKembali.')
            return
        except ValueError:
            print('Id tidak valid')
            self.delete()
        pass


def main():
    crud = CRUD()
    while True:
        os.system('clear')
        print('____CRUD____')
        print('1. Tambah Data')
        print('2. Lihat Data')
        print('3. Edit Data')
        print('4. Hapus Data')
        print('5. Keluar')
        choice = input('Pilih Menu(1 - 4): ')
        choice = int(choice)
        if choice in [1, 2, 3, 4]:
            os.system('clear')
            print('Tekan Control+C untuk kembali')
        if choice == 1:
            crud.create()
        elif choice == 2:
            crud.read()
        elif choice == 3:
            crud.update()
        elif choice == 4:
            crud.delete()
        elif choice == 5:
            print('Keluar.')
            break
        else:
            print('Gaje')
        try:
            input('Tekan Enter Untuk Melanjutkan')
        except KeyboardInterrupt:
            print('Keluar.')
            return


main()
